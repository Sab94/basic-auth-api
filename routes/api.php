<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('register/verify/{confirmationCode}','Api\Auth\RegisterController@confirm');
Route::post ( 'register', 'Api\Auth\RegisterController@register' );
Route::post ( 'login', 'Api\Auth\LoginController@login' );
