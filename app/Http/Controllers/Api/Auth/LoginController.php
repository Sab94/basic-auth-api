<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        $attempt = Auth::attempt($request->only('email', 'password'));

        if (!$attempt) {
            return response()->json([
                'error' => 'Unable to find an account matching the email address & password provided.'
            ], 400);
        }

        $user = User::where('email', $request->input('email'))->first();

        if(!$user->confirmed) {
            return response()->json([
                'error' => 'Email not verified'
            ], 400);
        }

        return response()->json(['user' => $user], 200);
    }
}
