<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Verify email</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc;">
    <tr>
        <td align="center" bgcolor="#5C342C" style="padding: 40px 0 30px 0;">

        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <h2>Verify Your Email Address</h2>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 20px 0 30px 0;">
                        Thanks for creating an account with the verification demo app. Please follow the <a href="{{config('app.spa_url').'/verify/' . $confirmation_code}}">link</a> below to verify your email address.
                        <br/>
                        <br/>
                        <br/>
                        <a href="{{config('app.spa_url').'/verify/' . $confirmation_code}}">Verify email</a>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <td bgcolor="#5C342C" style="padding: 30px 30px 30px 30px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>

            </tr>
        </table>
    </td>
</table>
</body>
</html>
