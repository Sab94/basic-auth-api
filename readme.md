# Basic Auth Api


## Getting Started
*API Starter*

-- Docker
* Make sure docker is installed and running
* Run: `sudo vi /etc/hosts` && add a host pointing to your local machine `127.0.0.1 basic-auth-api.local`
* Run: `composer install`
* Run: `cp .env.example .env` && make any changes necessary in that file
* Fill in the mail credentials ( for development mailtrap can be used)
```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
```
* Add `SPA_URL` in `.env`
```
SPA_URL=http://localhost:4200
```
* Run: `docker-compose build && docker-compose up`
* New Tab Run: `docker-compose exec app bash`
* You are now ssh'ed into the docker app and can run `php artisan migrate`

The api will now be running at: `http://basic-auth-api.local/api`.


