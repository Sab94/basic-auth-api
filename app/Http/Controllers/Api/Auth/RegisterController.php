<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => $validator->messages()
            ], 400);
        }

        //generate confirmation code
        $confirmation_code = str_random(30);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $confirmation_code,
        ]);

        Mail::send('email.verify-user',['confirmation_code' => $confirmation_code], function($message) {
            $message->to(Input::get('email'))
                ->subject('Verify your email address');
        });

        return response()->json(['user' => $user], 200);
    }

    public function confirm($confirmation_code)
    {
        if(!$confirmation_code)
        {
            return response()->json([
                'error' => 'Invalid confirmation code'
            ], 400);
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if (!$user)
        {
            return response()->json([
                'error' => 'No user found for provided confirmation code'
            ], 400);
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return response()->json(['user' => $user], 200);
    }
}
